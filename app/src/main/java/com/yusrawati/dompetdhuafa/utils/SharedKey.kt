package com.yusrawati.dompetdhuafa.utils

/**
 **********************************************
 * Created by ukie on 3/15/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */

object SharedKey {
    val idUser = "idUser"
    val onBoarding = "onBoarding"
    val typeUser = "typeUser"
    val is_login = "is_login"
    val userName = "userName"
    val statusNotif = "statusNotif"
    val tokenLogin = "tokenLogin"
    val tokenRefresh = "tokenRefresh"
}