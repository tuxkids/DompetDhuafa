package com.yusrawati.dompetdhuafa.utils

import android.app.Activity
import android.view.Gravity
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.yusrawati.dompetdhuafa.R

/**
 **********************************************
 * Created by ukie on 11/12/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
object CustomToast {
    private lateinit var toast: Toast
    fun makeText(activity: Activity, message: String?, duration: Int): Toast {
        toast = Toast(activity)
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.custom_toast, null)
        val ivLogoToast: ImageView = view.findViewById(R.id.iv_logo_toast)
        val tvToastMessage: TextView = view.findViewById(R.id.tv_toast_message)

        ivLogoToast.setImageResource(R.mipmap.ic_launcher)
        tvToastMessage.text = message

        toast.setGravity(Gravity.TOP, 0, activity.resources.getDimension(R.dimen.default_margin).toInt())
        toast.duration = duration
        toast.view = view
        toast.show()

        return toast
    }
}