package com.yusrawati.dompetdhuafa.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 **********************************************
 * Created by ukie on 9/5/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
object DateFormatter {
    private var inputTypeFormat = SimpleDateFormat()
    val outputType1 = SimpleDateFormat("yyyy-MM-dd", Locale("id", "ID"))
    val outputType2 = SimpleDateFormat("dd-MMMM-yyyy", Locale("id", "ID"))
    val outputType3 = SimpleDateFormat("EEEE, dd MMMM yyyy, HH:mm", Locale("id", "ID"))
    val outputType4 = SimpleDateFormat("dd MMMM yyyy ", Locale("id", "ID"))
    val outputType5 = SimpleDateFormat("M", Locale("id", "ID"))
    val outputType6 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale("id", "ID"))

    private var date: Date? = null
    private var output = ""

    fun format(inputDate: String, inputType: Int, outputType: Int): String {
        when (inputType) {
            1 -> inputTypeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale("id", "ID"))

            2 -> inputTypeFormat = SimpleDateFormat("yyyy-MM-dd", Locale("id", "ID"))
        }
        when (outputType) {
            1 -> {
                date = inputTypeFormat.parse(inputDate)
                output = outputType1.format(date)
            }

            2 -> {
                date = inputTypeFormat.parse(inputDate)
                output = outputType2.format(date)
            }

            3 -> {
                date = inputTypeFormat.parse(inputDate)
                output = outputType3.format(date)
            }
            4 -> {
                date = inputTypeFormat.parse(inputDate)
                output = outputType4.format(date)
            }
            5 -> {
                date = inputTypeFormat.parse(inputDate)
                output = outputType5.format(date)
            }
        }
        return output
    }
}