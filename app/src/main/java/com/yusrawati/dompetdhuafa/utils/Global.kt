package com.yusrawati.dompetdhuafa.utils

import okhttp3.ResponseBody
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 **********************************************
 * Created by ukie on 8/30/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
object Global {
    var tokenGeneral: String? = ""
    val auth: String = "Authorization"
    val calendar = Calendar.getInstance(Locale.getDefault())

    fun parseError(response: ResponseBody): String {
        return try {
            val jsonObject = JSONObject(response.source().readUtf8())
            val description: JSONObject = jsonObject.getJSONObject("diagnostic")
            description.getString("description")
        } catch (e: Exception) {
            e.message.toString()
        }
    }

    //    fun logout(activity: Activity) {
//        val logout = AlertDialog.Builder(activity)
//        logout.setTitle(activity.getString(R.string.alert))
//        logout.setMessage(activity.getString(R.string.session_expired))
//        logout.setPositiveButton(activity.getString(R.string.login), { dialogInterface, _ ->
//            SharedPref.saveBol(SharedKey.is_login, false)
//            activity.finish()
//            activity.startActivity(Intent(activity, VLogin::class.java))
//            dialogInterface.dismiss()
//        }).setCancelable(false)
//        logout.show()
//    }
    fun currencyFormat(currency: Double): String {
        var currencyFormat = NumberFormat.getCurrencyInstance(Locale("id")) as DecimalFormat
        val decimalFormatSymbols = currencyFormat.decimalFormatSymbols
        decimalFormatSymbols.currencySymbol = "Rp. "
        currencyFormat.decimalFormatSymbols = decimalFormatSymbols
        currencyFormat.maximumFractionDigits = 0
        return currencyFormat.format(currency)
    }

    fun convertTimestamp(timeStamp: Long): String {
        val date = Date(timeStamp * 1000)
        DateFormatter.outputType6.timeZone = TimeZone.getTimeZone("GMT+8")
        return DateFormatter.outputType6.format(date)
    }

}