package com.yusrawati.dompetdhuafa.utils

import android.app.Application
import android.content.Context
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger

/**
 *----------------------------------------------
 * Created by ukieTux on 2/24/17 with ♥ .
 * @email  : ukie.tux@gmail.com
 * @github : https://www.github.com/tuxkids
 * ----------------------------------------------
 *          © 2017 | All Rights Reserved
 */
class AppController : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        Logger.addLogAdapter(AndroidLogAdapter())
    }


    companion object {
        var instance: AppController? = null
            private set
        var context: Context? = null
            private set
    }
}
