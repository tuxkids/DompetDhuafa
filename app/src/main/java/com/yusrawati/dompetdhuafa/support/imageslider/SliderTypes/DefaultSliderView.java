package com.yusrawati.dompetdhuafa.support.imageslider.SliderTypes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.yusrawati.dompetdhuafa.R;

/**
 * a simple slider view, which just show an image. If you want to make your own slider view,
 * <p>
 * just extend BaseSliderView, and implement getView() method.
 */
public class DefaultSliderView extends BaseSliderView {

    public DefaultSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.image_slider_render_type_default, null);
        ImageView target = v.findViewById(R.id.daimajia_slider_image);
        ProgressBar progressBar = v.findViewById(R.id.loading_bar);
        bindEventAndShow(v, target, progressBar);
        return v;
    }
}
