package com.yusrawati.dompetdhuafa.support.datepicker;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;

import com.yusrawati.dompetdhuafa.R;
import com.yusrawati.dompetdhuafa.support.datepicker.wheel.ArrayWheelAdapter;
import com.yusrawati.dompetdhuafa.support.datepicker.wheel.NumericWheelAdapter;
import com.yusrawati.dompetdhuafa.support.datepicker.wheel.OnWheelChangedListener;
import com.yusrawati.dompetdhuafa.support.datepicker.wheel.WheelView;

import java.util.Calendar;

//import com.orhanobut.logger.Logger;

public class DatePickerDailog extends Dialog {

    private Context mContext;

    private int NoOfYear = 70;

    Button btnSet, btnCancel;

    public DatePickerDailog(Context context, Calendar calendar,
                            final DatePickerListner dtp) {
        super(context);

//        NoOfYear = minYear;
//        NoOfYear = maxYear;
//        super(context);
        mContext = context;
        LinearLayout layoutMain = new LinearLayout(mContext);
        layoutMain.setOrientation(LinearLayout.VERTICAL);
        layoutMain.setBackgroundColor(Color.WHITE);
        LinearLayout layoutDate = new LinearLayout(mContext);
        LinearLayout layoutButton = new LinearLayout(mContext);
        layoutButton.setBackgroundResource(R.drawable.bg_button_primary);


        btnCancel = new Button(mContext);
        btnCancel.setBackgroundResource(R.drawable.bg_button_primary);
        btnSet = new Button(mContext);
        btnSet.setBackgroundResource(R.drawable.bg_button_primary);

        btnCancel.setText(context.getString(R.string.cancel));
        btnCancel.setTextColor(Color.WHITE);
        btnSet.setText(context.getString(R.string.select));
        btnSet.setTextColor(Color.WHITE);

        final WheelView month = new WheelView(mContext);
        final WheelView year = new WheelView(mContext);
        final WheelView day = new WheelView(mContext);


        layoutDate.addView(day, new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1.2f));
        layoutDate.addView(month, new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 0.8f));
        layoutDate.addView(year, new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        layoutButton.addView(btnCancel, new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        layoutButton.addView(btnSet, new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        layoutMain.addView(layoutDate);
        layoutMain.addView(layoutButton);


        setContentView(layoutMain);

        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER_VERTICAL);
        OnWheelChangedListener listener = new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                updateDays(year, month, day);

            }
        };

        // month
        int curMonth = calendar.get(Calendar.MONTH);
        String months[] = new String[]{"Januari", "Februari", "Maret",
                "April", "Mei", "Juni", "Juli", "Agustus", "September",
                "Oktober", "November", "Desember"};
//        String setMonth[] = Arrays.copyOf(months, curMonth - 1);
//        Logger.d(setMonth + "> nama bulan");
//        month.setViewAdapter(new DateNumericAdapter(context, curMonth + curMonth, 1));
        month.setViewAdapter(new DateArrayAdapter(context, months, curMonth));
        month.setCurrentItem(curMonth);
        month.addChangingListener(listener);

        Calendar cal = Calendar.getInstance();

        // year
        int curYear = calendar.get(Calendar.YEAR);
        int Year = cal.get(Calendar.YEAR);


        year.setViewAdapter(new DateNumericAdapter(context, Year - NoOfYear,
                Year + NoOfYear, NoOfYear));
        year.setCurrentItem(curYear - (Year - NoOfYear));
        year.addChangingListener(listener);

        // day
        updateDays(year, month, day);
        day.setCurrentItem(calendar.get(Calendar.DAY_OF_MONTH) - 1);


        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dtp.onCancelButton(DatePickerDailog.this);

            }
        });

        btnSet.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar c = updateDays(year, month, day);
                dtp.onDoneButton(DatePickerDailog.this, c);
            }
        });

    }

    Calendar updateDays(WheelView year, WheelView month, WheelView day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,
                calendar.get(Calendar.YEAR) + (year.getCurrentItem() - NoOfYear));
        calendar.set(Calendar.MONTH, month.getCurrentItem());

        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        day.setViewAdapter(new DateNumericAdapter(mContext, 1, maxDays, calendar
                .get(Calendar.DAY_OF_MONTH) - 1));
        int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
        day.setCurrentItem(curDay - 1, true);
        calendar.set(Calendar.DAY_OF_MONTH, curDay);
        return calendar;

    }

    public void setMaxDate(Calendar maxDate) {
        maxDate = maxDate;
    }

    public void setMinDate(Calendar minDate) {
        minDate = minDate;
    }


    private class DateNumericAdapter extends NumericWheelAdapter {
        int currentItem;
        int currentValue;

        public DateNumericAdapter(Context context, int minValue, int maxValue,
                                  int current) {
            super(context, minValue, maxValue);
            this.currentValue = current;
            setTextSize(20);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF000000);
            }
            view.setTypeface(null, Typeface.NORMAL);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    private class DateArrayAdapter extends ArrayWheelAdapter<String> {
        int currentItem;
        int currentValue;

        public DateArrayAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
            setTextSize(20);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF000000);
            }
            view.setTypeface(null, Typeface.NORMAL);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    public interface DatePickerListner {
        void onDoneButton(Dialog dateDialog, Calendar c);

        void onCancelButton(Dialog dateDialog);
    }
}