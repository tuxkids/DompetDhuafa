package com.yusrawati.dompetdhuafa.support.imageslider.SliderTypes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.yusrawati.dompetdhuafa.R;


/**
 * This is a slider with a titleDescription TextView.
 */
public class TextSliderView extends BaseSliderView {
    public TextSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.image_slider_render_type_text, null);
        ImageView target = v.findViewById(R.id.daimajia_slider_image);
        TextView titleDescription = v.findViewById(R.id.tv_title_description);
        ProgressBar progressBar = v.findViewById(R.id.loading_bar);
        titleDescription.setText(getDescription());
//        TextView contentDescription = (TextView) v.findViewById(R.id.tv_content_description);
//        contentDescription.setText(getContentDescription());
        bindEventAndShow(v, target, progressBar);
        return v;
    }
}
