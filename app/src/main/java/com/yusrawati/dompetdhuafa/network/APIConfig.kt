package com.yusrawati.dompetdhuafa.network

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.yusrawati.dompetdhuafa.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 *----------------------------------------------
 * Created by ukieTux on 2/28/17 with ♥ .
 * @email  : ukie.tux@gmail.com
 * @github : https://www.github.com/tuxkids
 * ----------------------------------------------
 *          © 2017 | All Rights Reserved
 */


object api {

    private val okHttpClient: OkHttpClient? = OkhttpClient.instance?.okHttpClient
    private val cariBusClient = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient ?: throw Exception())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .addConverterFactory(NullOnEmptyConverterFactory())
            .build() ?: throw NullPointerException()

    val API_CLIENT: DompetDhuafaClient = cariBusClient.create(DompetDhuafaClient::class.java)
}