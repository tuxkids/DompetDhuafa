package com.yusrawati.dompetdhuafa.network

import com.yusrawati.dompetdhuafa.models.*
import io.reactivex.Flowable
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * ----------------------------------------------
 * Created by ukieTux on 2/28/17 with ♥ .
 * @email : ukie.tux@gmail.com
 * @github : https://www.github.com/tuxkids
 * * ----------------------------------------------
 * * © 2017 | All Rights Reserved
 */

interface DompetDhuafaClient {

    @GET("generate/token/general")
    fun getGeneralToken(): Flowable<Response<DataGeneralToken>>

    @GET("api/generate/token/login")
    fun getLogin(): Flowable<Response<DataLogin>>

    @POST("api/register")
    fun postRegister(): Flowable<Response<DataRegister>>

    @GET("api/ngurban/gethewan")
    fun getHewan(): Flowable<Response<DataGetHewan>>

    @POST("api/ngurban/buatformulir")
    fun postFormulir(): Flowable<Response<DataPostFormulir.PostFormulir>>

    @GET("api/ngurban/riwayatformulir")
    fun getHistory(): Flowable<Response<DataHistory.History>>

    @Multipart
    @POST("api/ngurban/konfirmasipembayaran")
    fun postKonfirmasiPembayaran(@Part image: MultipartBody.Part): Flowable<Response<DiagnosticOnly>>

    @GET("api/berita")
    fun getBerita(): Flowable<Response<DataModelBerita.DataBerita>>
}
