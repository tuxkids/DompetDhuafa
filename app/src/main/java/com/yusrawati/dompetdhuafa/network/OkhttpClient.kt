package com.yusrawati.dompetdhuafa.network

import com.orhanobut.logger.Logger
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

/**
 * ----------------------------------------------
 * Created by ukieTux on 5/24/16 with ♥ .
 * @email : ukie.tux@gmail.com
 * *
 * @github : https://www.github.com/tuxkids
 * * ----------------------------------------------
 * * © 2017 | All Rights Reserved
 */
class OkhttpClient private constructor() {

    var okHttpClient: OkHttpClient? = null
    private var headers: LinkedHashMap<String, String>? = null

    fun setHeaders(headers: LinkedHashMap<String, String>) {
        this.headers = headers
    }

    init {
        //Logging -> hapus saat release
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
        builder.connectTimeout(60, TimeUnit.SECONDS)
        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.writeTimeout(60, TimeUnit.SECONDS)
        builder.retryOnConnectionFailure(true)
        builder.addInterceptor(logging)
        builder.addInterceptor(interceptor)

        okHttpClient = builder.build()
        okHttpClient = okHttpClient
    }

    private val interceptor: Interceptor
        get() {
            headers = LinkedHashMap()
            return Interceptor { chain ->
                val original = chain.request()
                val newRequest = original.newBuilder()

                headers?.keys?.filter { headers!![it] != null }
                        ?.forEach {
                            try {
                                newRequest.addHeader(it, headers!![it])
                                Logger.d("key :$it value : ${headers!![it]}")
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                chain.proceed(newRequest.build())
            }
        }

    companion object {
        private var ourInstance: OkhttpClient? = null


        var instance: OkhttpClient? = null
            get() {

                if (ourInstance == null) {
                    ourInstance = OkhttpClient()
                }

                return ourInstance!!
            }
    }

    //    private static final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
    //        @Override
    //        public Response intercept(Chain chain) throws IOException {
    //            CacheControl.Builder cacheBuilder = new CacheControl.Builder();
    //            cacheBuilder.maxAge(0, TimeUnit.SECONDS);
    //            cacheBuilder.maxStale(365, TimeUnit.DAYS);
    //            CacheControl cacheControl = cacheBuilder.build();
    //
    //            Request request = chain.request();
    //            if (ConnectionReceiver.isConnected()) {
    //                request = request.newBuilder()
    //                        .cacheControl(cacheControl)
    //                        .build();
    //            }
    //            Response originalResponse = chain.proceed(request);
    //            if (ConnectionReceiver.isConnected()) {
    //                int maxAge = 0;// read from cache
    //                return originalResponse.newBuilder()
    //                        .header("Cache-Control", "public, max-age=" + maxAge)
    //                        .build();
    //            } else {
    //                int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
    //                return originalResponse.newBuilder()
    //                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
    //                        .build();
    //            }
    //        }
    //    };

}