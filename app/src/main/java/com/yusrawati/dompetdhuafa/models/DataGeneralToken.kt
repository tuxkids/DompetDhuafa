package com.yusrawati.dompetdhuafa.models

/**
 **********************************************
 * Created by ukie on 8/30/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */

class DataGeneralToken {
    var diagnostic: Diagnostic? = null
    var response: Response? = null

    data class Response(
            var token_type: String? = "",
            var expires_in: Int? = 0,
            var access_token: String? = "",
            var version: String? = "",
            var link: String? = ""
    )
}