package com.yusrawati.dompetdhuafa.models

/**
 **********************************************
 * Created by ukie on 8/30/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class DiagnosticOnly {
    var diagnostic: Diagnostic? = null
}