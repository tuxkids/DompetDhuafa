package com.yusrawati.dompetdhuafa.models

import android.os.Parcel
import android.os.Parcelable

data class Diagnostic(
        val memoryusage: String? = "",
        val description: String? = "",
        val elapstime: String? = "",
        val status: Int? = 0,
        val timestamp: Int? = 0
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(memoryusage)
        writeString(description)
        writeString(elapstime)
        writeValue(status)
        writeValue(timestamp)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Diagnostic> = object : Parcelable.Creator<Diagnostic> {
            override fun createFromParcel(source: Parcel): Diagnostic = Diagnostic(source)
            override fun newArray(size: Int): Array<Diagnostic?> = arrayOfNulls(size)
        }
    }
}
