package com.yusrawati.dompetdhuafa.models

import android.os.Parcel
import android.os.Parcelable

/**
 **********************************************
 * Created by ukie on 11/14/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class DataPostFormulir {

    data class PostFormulir(
            val diagnostic: Diagnostic? = null,
            val bank_tujuan: List<DataHistory.BankTujuan>? = null,
            val response: DataHistory.Response? = null
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readParcelable<Diagnostic>(Diagnostic::class.java.classLoader),
                source.createTypedArrayList(DataHistory.BankTujuan.CREATOR),
                source.readParcelable<DataHistory.Response>(DataHistory.Response::class.java.classLoader)
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeParcelable(diagnostic, 0)
            writeTypedList(bank_tujuan)
            writeParcelable(response, 0)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<PostFormulir> = object : Parcelable.Creator<PostFormulir> {
                override fun createFromParcel(source: Parcel): PostFormulir = PostFormulir(source)
                override fun newArray(size: Int): Array<PostFormulir?> = arrayOfNulls(size)
            }
        }
    }
}