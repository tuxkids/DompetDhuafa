package com.yusrawati.dompetdhuafa.models

/**
 **********************************************
 * Created by ukie on 11/12/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */

class DataLogin {
    var diagnostic: Diagnostic? = null
    var response: Response? = null

    data class Response(
            var token_type: String? = "",
            var expires_in: Int? = 0,
            var access_token: String? = "",
            var refresh_token: String? = "",
            var user: User? = null
    )

    data class User(
            var id: Int? = 0,
            var username: String? = "",
            var nomor_tlp: String? = "",
            var email: String? = "",
            var verification: String? = "",
            var apps: String? = ""
    )
}