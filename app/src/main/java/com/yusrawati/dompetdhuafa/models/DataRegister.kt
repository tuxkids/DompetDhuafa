package com.yusrawati.dompetdhuafa.models

/**
 **********************************************
 * Created by ukie on 11/12/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */

class DataRegister {
    var diagnostic: Diagnostic? = null
    var response: Response? = null

    data class Response(
            var user: User? = null,
            var profile: Profile? = null
    )

    data class User(
            var username: String? = "",
            var nomor_tlp: String? = "",
            var email: String? = "",
            var id: Int? = 0
    )

    data class Profile(
            var users_id: String? = "",
            var nama_lengkap: String? = "",
            var tgl_lahir: String? = "",
            var jns_kelamin: String? = "",
            var alamat: String? = ""
    )
}