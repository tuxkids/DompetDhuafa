package com.yusrawati.dompetdhuafa.models

import android.os.Parcel
import android.os.Parcelable

/**
 **********************************************
 * Created by ukie on 11/13/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class DataGetHewan {
    val diagnostic: Diagnostic? = null
    val bank_tujuan: List<bankTujuan>? = null
    val response: List<Response>? = null

    data class bankTujuan(
            val id: String? = "",
            val bank: String? = "",
            val nomor_rekening: String? = ""
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(bank)
            writeString(nomor_rekening)
        }

        override fun toString(): String {
            return bank ?: ""
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<bankTujuan> = object : Parcelable.Creator<bankTujuan> {
                override fun createFromParcel(source: Parcel): bankTujuan = bankTujuan(source)
                override fun newArray(size: Int): Array<bankTujuan?> = arrayOfNulls(size)
            }
        }
    }

    data class Response(
            val id: String? = "",
            val nama: String? = "",
            val max_kuota: Int? = 0,
            val harga: Double? = 0.0

    ) : Parcelable {
        override fun toString(): String {
            return nama ?: ""
        }

        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readValue(Int::class.java.classLoader) as Int?,
                source.readValue(Double::class.java.classLoader) as Double?
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(nama)
            writeValue(max_kuota)
            writeValue(harga)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<Response> = object : Parcelable.Creator<Response> {
                override fun createFromParcel(source: Parcel): Response = Response(source)
                override fun newArray(size: Int): Array<Response?> = arrayOfNulls(size)
            }
        }
    }
}