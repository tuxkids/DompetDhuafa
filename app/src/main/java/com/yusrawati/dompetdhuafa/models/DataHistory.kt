package com.yusrawati.dompetdhuafa.models

import android.os.Parcel
import android.os.Parcelable

/**
 **********************************************
 * Created by ukie on 11/14/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class DataHistory {

    data class History(
            val diagnostic: Diagnostic? = null,
            val bank_tujuan: List<BankTujuan>? = null,
            val response: List<Response>? = null
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readParcelable<Diagnostic>(Diagnostic::class.java.classLoader),
                source.createTypedArrayList(BankTujuan.CREATOR),
                source.createTypedArrayList(Response.CREATOR)
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeParcelable(diagnostic, 0)
            writeTypedList(bank_tujuan)
            writeTypedList(response)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<History> = object : Parcelable.Creator<History> {
                override fun createFromParcel(source: Parcel): History = History(source)
                override fun newArray(size: Int): Array<History?> = arrayOfNulls(size)
            }
        }
    }

    data class Response(
            val id: String? = "",
            val id_hewan_kurban: String? = "",
            val tanggal: String? = "",
            val jml_orang: String? = "",
            val total: Double? = 0.0,
            val kodeunik: String? = "",
            val users_id: String? = "",
            val expire: String? = "",
            val fl_status: Int? = 0,
            val status: String? = "",
            val hewankurban: Hewan? = null,
            val detail: List<Detail>? = null
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.readValue(Double::class.java.classLoader) as Double?,
                source.readString(),
                source.readString(),
                source.readString(),
                source.readValue(Int::class.java.classLoader) as Int?,
                source.readString(),
                source.readParcelable<Hewan>(Hewan::class.java.classLoader),
                source.createTypedArrayList(Detail.CREATOR)
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(id_hewan_kurban)
            writeString(tanggal)
            writeString(jml_orang)
            writeValue(total)
            writeString(kodeunik)
            writeString(users_id)
            writeString(expire)
            writeValue(fl_status)
            writeString(status)
            writeParcelable(hewankurban, 0)
            writeTypedList(detail)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<Response> = object : Parcelable.Creator<Response> {
                override fun createFromParcel(source: Parcel): Response = Response(source)
                override fun newArray(size: Int): Array<Response?> = arrayOfNulls(size)
            }
        }
    }

    data class Hewan(
            val id: String? = "",
            val nama: String? = "",
            val max_kuota: Int? = 0,
            val harga: Double? = 0.0
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readValue(Int::class.java.classLoader) as Int?,
                source.readValue(Double::class.java.classLoader) as Double?
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(nama)
            writeValue(max_kuota)
            writeValue(harga)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<Hewan> = object : Parcelable.Creator<Hewan> {
                override fun createFromParcel(source: Parcel): Hewan = Hewan(source)
                override fun newArray(size: Int): Array<Hewan?> = arrayOfNulls(size)
            }
        }
    }

    data class Detail(
            val id_transaksi: String? = "",
            val id: String? = "",
            val nama: String? = "",
            val harga: Double? = 0.0
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readString(),
                source.readValue(Double::class.java.classLoader) as Double?
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id_transaksi)
            writeString(id)
            writeString(nama)
            writeValue(harga)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<Detail> = object : Parcelable.Creator<Detail> {
                override fun createFromParcel(source: Parcel): Detail = Detail(source)
                override fun newArray(size: Int): Array<Detail?> = arrayOfNulls(size)
            }
        }
    }

    data class BankTujuan(
            val id: String? = "",
            val bank: String? = "",
            val nomor_rekening: String? = ""
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(bank)
            writeString(nomor_rekening)
        }

        override fun toString(): String {
            return "$bank - $nomor_rekening"
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<BankTujuan> = object : Parcelable.Creator<BankTujuan> {
                override fun createFromParcel(source: Parcel): BankTujuan = BankTujuan(source)
                override fun newArray(size: Int): Array<BankTujuan?> = arrayOfNulls(size)
            }
        }
    }
}