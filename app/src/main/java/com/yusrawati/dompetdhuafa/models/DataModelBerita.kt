package com.yusrawati.dompetdhuafa.models

import android.os.Parcel
import android.os.Parcelable

/**
 **********************************************
 * Created by ukie on 12/9/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class DataModelBerita {

    data class DataBerita(
            val diagnostic: Diagnostic? = null,
            val response: List<Response>? = null
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readParcelable<Diagnostic>(Diagnostic::class.java.classLoader),
                ArrayList<Response>().apply { source.readList(this, Response::class.java.classLoader) }
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeParcelable(diagnostic, 0)
            writeList(response)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<DataBerita> = object : Parcelable.Creator<DataBerita> {
                override fun createFromParcel(source: Parcel): DataBerita = DataBerita(source)
                override fun newArray(size: Int): Array<DataBerita?> = arrayOfNulls(size)
            }
        }
    }

    data class Response(
            val id: String? = "",
            val judul: String? = "",
            val gambar: String? = "",
            val isi: String? = "",
            val tanggal: String? = "",
            val sumber: String? = "",
            val operator: String? = "",
            val fl_status: String? = ""
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(judul)
            writeString(gambar)
            writeString(isi)
            writeString(tanggal)
            writeString(sumber)
            writeString(operator)
            writeString(fl_status)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<Response> = object : Parcelable.Creator<Response> {
                override fun createFromParcel(source: Parcel): Response = Response(source)
                override fun newArray(size: Int): Array<Response?> = arrayOfNulls(size)
            }
        }
    }
}