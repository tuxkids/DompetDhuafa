package com.yusrawati.dompetdhuafa.ui.ngurban

import com.yusrawati.dompetdhuafa.base.BaseMVP
import com.yusrawati.dompetdhuafa.models.DataGetHewan
import com.yusrawati.dompetdhuafa.models.DataPostFormulir

/**
 **********************************************
 * Created by ukie on 11/13/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
interface INgurban {
    interface View : BaseMVP {
        fun onGetHewan(dataGetHewan: DataGetHewan)
        fun onPostFormulir(dataPostFormulir: DataPostFormulir.PostFormulir)
    }

    interface Presenter {
        fun getHewan(headers: LinkedHashMap<String, String>)
        fun postFormulir(headers: LinkedHashMap<String, String>)
    }
}