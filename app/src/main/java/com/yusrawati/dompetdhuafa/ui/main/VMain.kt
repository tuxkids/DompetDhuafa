package com.yusrawati.dompetdhuafa.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.widget.Toast
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataModelBerita
import com.yusrawati.dompetdhuafa.support.imageslider.Animations.DescriptionAnimation
import com.yusrawati.dompetdhuafa.support.imageslider.SliderLayout
import com.yusrawati.dompetdhuafa.support.imageslider.SliderTypes.TextSliderView
import com.yusrawati.dompetdhuafa.ui.berita.VBerita
import com.yusrawati.dompetdhuafa.ui.berita.detail.VDetailBerita
import com.yusrawati.dompetdhuafa.ui.history.VHistory
import com.yusrawati.dompetdhuafa.ui.login.VLogin
import com.yusrawati.dompetdhuafa.ui.ngurban.VNgurban
import com.yusrawati.dompetdhuafa.utils.CustomToast
import com.yusrawati.dompetdhuafa.utils.Global
import com.yusrawati.dompetdhuafa.utils.SharedKey
import com.yusrawati.dompetdhuafa.utils.SharedPref
import kotlinx.android.synthetic.main.activity_main_screen.*


class VMain : BaseActivity(), IMain.View {
    private val mPresenter = PMain()
    override fun getLayoutResource(): Int = R.layout.activity_main_screen



    override fun myCodeHere() {
        /**
         * TODO Cek sudah login atau belum di sharedpreference
         */
        if (SharedPref.getBol(SharedKey.is_login)) {
            setupView()
        } else {
            finish()
            startActivity(Intent(applicationContext, VLogin::class.java))
        }
    }


    @SuppressLint("RestrictedApi")
    override fun setupView() {
        iv_history.setOnClickListener {
            startActivity(Intent(applicationContext, VHistory::class.java))
        }
        btn_qurban.setOnClickListener {
            startActivity(Intent(applicationContext, VNgurban::class.java))
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        /**
         * TODO Ambil Berita
         * Ambil data berita
         */
        mPresenter.attachView(this)
        val headers = LinkedHashMap<String, String>()
        headers.put(Global.auth, SharedPref.getString(SharedKey.tokenLogin))
        mPresenter.getBerita(headers)

        iv_history.setOnClickListener {
            startActivity(Intent(applicationContext, VHistory::class.java))
        }

        btn_qurban.setOnClickListener {
            startActivity(Intent(applicationContext, VNgurban::class.java))
        }

    }

    override fun onDialog(show: Boolean) {
    }

    override fun onError(message: String) {
        CustomToast.makeText(this, message, Toast.LENGTH_SHORT)
    }

    override fun onExpireToken() {

    }

    /**
     * TODO Respon Berita dari server
     */
    override fun onGetBerita(dataBerita: DataModelBerita.DataBerita) {
        sl_news.removeAllSliders()

        /**
         * TODO Limit berita di slider
         */
        var limit = if (dataBerita.response?.size!! > 5) 5 else dataBerita.response?.size

        for (i in 0 until limit) {
            val tsv = TextSliderView(this)

            tsv.image(dataBerita.response?.get(i)?.gambar)
                    .titleDescription(dataBerita.response?.get(i)?.judul)
                    .setOnSliderClickListener { // TODO Onclick ImageSlider
                        val detailNews = Intent(applicationContext, VDetailBerita::class.java)
                        detailNews.putExtra("response", dataBerita.response[i])
                        startActivity(detailNews)
                    }
            sl_news.addSlider(tsv)
            sl_news.isEnabled = false
        }
        sl_news.setPresetTransformer(SliderLayout.Transformer.Default)
        sl_news.setCustomAnimation(DescriptionAnimation())
        sl_news.setDuration(4000) // TODO Durasi ImageSlider

        btn_news.setOnClickListener { // TODO OnClick Berita
            val berita = Intent(applicationContext, VBerita::class.java)
            berita.putExtra("berita", dataBerita)
            startActivity(berita)
        }

    }

    override fun onDetachScreen() {
        mPresenter.detachView()
    }
}
