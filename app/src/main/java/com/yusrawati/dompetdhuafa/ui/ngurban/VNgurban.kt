package com.yusrawati.dompetdhuafa.ui.ngurban

import android.content.Intent
import android.os.Parcelable
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.orhanobut.logger.Logger
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataGetHewan
import com.yusrawati.dompetdhuafa.models.DataPostFormulir
import com.yusrawati.dompetdhuafa.ui.konfirmasi.VKonfirmasi
import com.yusrawati.dompetdhuafa.utils.CustomToast
import com.yusrawati.dompetdhuafa.utils.Global
import com.yusrawati.dompetdhuafa.utils.SharedKey
import com.yusrawati.dompetdhuafa.utils.SharedPref
import kotlinx.android.synthetic.main.activity_ngurban_screen.*
import kotlinx.android.synthetic.main.custom_spinner.view.*
import java.util.ArrayList
import kotlin.collections.LinkedHashMap

class VNgurban : BaseActivity(), INgurban.View {
    private val mPresenter = PNgurban()
    private var maxJumlah = 1
    private var idHewan = ""
    private var jmlKurban = 0
    private var hargaPerorang = 0.0

    override fun getLayoutResource(): Int = R.layout.activity_ngurban_screen

    override fun myCodeHere() {
        title = getString(R.string.form_qurban)
        setupView()
    }

    override fun setupView() {
        mPresenter.attachView(this)

        //TODO Get list hewan kurban
        val headers = LinkedHashMap<String, String>()
        headers.put(Global.auth, SharedPref.getString(SharedKey.tokenLogin))
        mPresenter.getHewan(headers)

        /**
         * TODO Mendeteksi perubahan di jumlah pekurban
         */
        tet_jumlah_pekurban.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s == "0" || s!!.isEmpty()) {
                    tet_jumlah_pekurban.setText("1")
                    jmlKurban = 1
                    generatePekurban(jmlKurban)
                } else {
                    jmlKurban = s.toString().toInt()
                    /**
                     * TODO Seleksi jika melibih maximal jumlah pekurban
                     */
                    if (jmlKurban > maxJumlah) {
                        tet_jumlah_pekurban.error = "Melebih jumlah maximal pekurban"
                        ll_root.removeAllViews()
                    } else {
                        generatePekurban(jmlKurban)
                        tet_jumlah_pekurban.error = null
                    }
                }
                tv_total_harga.text = "Total Bayar : ${Global.currencyFormat(hargaPerorang * jmlKurban)}"
            }
        })

        btn_next.setOnClickListener {
            /**
             * TODO cek jika jumlah pekurban tidak kosong
             */
            if (tet_jumlah_pekurban.text.isNotEmpty()) {
                tet_jumlah_pekurban.error = null
                var isValid = false

                /**
                 * TODO cek semua nama pekurban terisi atau tidak
                 */
                loop@ for (item in 1..tet_jumlah_pekurban.text.toString().toInt()) {
                    val editText: TextInputEditText = findViewById(item)
                    isValid = editText.text.toString().isNotEmpty()
                    if (!isValid)
                        break@loop
                }

                //TODO Submit form keserver
                if (isValid) {
                    val headers = LinkedHashMap<String, String>()
                    headers.put(Global.auth, SharedPref.getString(SharedKey.tokenLogin))
                    headers.put("idhewankurban", idHewan)
                    headers.put("jmlorang", tet_jumlah_pekurban.text.toString())
                    /**
                     * TODO Looping nama pekurban untuk dikirim ke server
                     */
                    for (item in 1..tet_jumlah_pekurban.text.toString().toInt()) {
                        val editText: TextInputEditText = findViewById(item)
                        headers.put("nama$item", editText.text.toString())
                    }
                    mPresenter.postFormulir(headers)
                } else {
                    onError(getString(R.string.error_empty))
                }
            } else {
                tet_jumlah_pekurban.error = "Masukkan jumlah pekurban"
            }
        }
    }

    override fun onDialog(show: Boolean) {
        pb_loading.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onError(message: String) {
        CustomToast.makeText(this, message, Toast.LENGTH_SHORT)
    }

    override fun onExpireToken() {
    }

    override fun onGetHewan(dataGetHewan: DataGetHewan) {
        sv_root.visibility = View.VISIBLE

        val adapterHewan = ArrayAdapter<DataGetHewan.Response>(this, android.R.layout.simple_spinner_item, dataGetHewan.response)
        adapterHewan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        cs_hewan.sp_content.adapter = adapterHewan
        cs_hewan.sp_content.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                idHewan = dataGetHewan.response?.get(position)?.id ?: "1"
                maxJumlah = dataGetHewan.response?.get(position)?.max_kuota ?: 1
                hargaPerorang = dataGetHewan.response?.get(position)?.harga ?: 0.0
                tv_harga_perorang.text = "Bayar Perorang : ${Global.currencyFormat(hargaPerorang)}"
                tv_total_harga.text = "Total Bayar : ${Global.currencyFormat(hargaPerorang * jmlKurban)}"


                //TODO Reset jumlah pekurban tiap ganti pilihan
                jmlKurban = 1
                generatePekurban(jmlKurban)
            }
        }
    }

    //TODO Menerima respon submit  dari server
    override fun onPostFormulir(dataPostFormulir: DataPostFormulir.PostFormulir) {
        onError(dataPostFormulir.diagnostic?.description ?: "")
        val konfirmasi = Intent(applicationContext, VKonfirmasi::class.java)
        konfirmasi.putExtra("response", dataPostFormulir.response)
        konfirmasi.putParcelableArrayListExtra("bank", dataPostFormulir.bank_tujuan as ArrayList<out Parcelable>)
        startActivity(konfirmasi)
    }

    /**
     * TODO Dynamic Edit Text Berdasarkan jumlah pekurban
     */
    private fun generatePekurban(jumlah: Int) {
        ll_root.removeAllViews()
        Logger.d(jumlah)
        for (item in 1..jumlah) {
            //create textInputLayout
            val textInputLayout = TextInputLayout(this)
            ll_root.addView(textInputLayout)

            //create textInputEditText
            val textInputEditText = TextInputEditText(this)
            textInputEditText.hint = "Nama Pekurban$item"
            textInputEditText.id = item
            textInputEditText.setSingleLine(true)
            textInputLayout.addView(textInputEditText)
        }
    }

    override fun onDetachScreen() {
        mPresenter.detachView()
    }
}
