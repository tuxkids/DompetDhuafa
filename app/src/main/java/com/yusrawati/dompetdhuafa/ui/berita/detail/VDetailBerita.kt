package com.yusrawati.dompetdhuafa.ui.berita.detail

import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataModelBerita
import kotlinx.android.synthetic.main.activity_berita_detail_screen.*

class VDetailBerita : BaseActivity() {

    override fun getLayoutResource(): Int = R.layout.activity_berita_detail_screen

    override fun myCodeHere() {
        title = getString(R.string.detail_berita)

        val response: DataModelBerita.Response = intent.getParcelableExtra("response")
        tv_title.text = response.judul
        tv_date.text = response.tanggal
        tv_writer.text = response.sumber
        tv_content_news.text = response.isi

        Glide.with(this)
                .load(response.gambar)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv_news)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            supportFinishAfterTransition()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onDetachScreen() {
        supportStartPostponedEnterTransition()
    }
}
