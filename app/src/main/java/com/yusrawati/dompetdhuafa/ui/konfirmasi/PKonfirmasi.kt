package com.yusrawati.dompetdhuafa.ui.konfirmasi

import com.yusrawati.dompetdhuafa.base.BasePresenter
import com.yusrawati.dompetdhuafa.network.OkhttpClient
import com.yusrawati.dompetdhuafa.network.api
import com.yusrawati.dompetdhuafa.utils.Global
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 **********************************************
 * Created by ukie on 11/21/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class PKonfirmasi : BasePresenter<IKonfirmasi.View>(), IKonfirmasi.Presenter {
    private val composite = CompositeDisposable()
    override fun detachView() {
        super.detachView()
        composite.dispose()
    }

    override fun postKonfirmasiPembayaran(headers: LinkedHashMap<String, String>, file: File) {
        OkhttpClient.instance?.setHeaders(headers)
        val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
        val body = MultipartBody.Part.createFormData("foto", file.name, requestFile)
        composite.add(api.API_CLIENT.postKonfirmasiPembayaran(body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { mView()?.onDialog(true) }
                .subscribe { it ->
                    if (it.code() == 200) {
                        mView()?.onDialog(false)
                        mView()?.onPostKonfirmasiPembayaran(it.body()?.diagnostic!!)
                    } else {
                        mView()?.onError(Global.parseError(it.errorBody()!!))
                    }
                })
    }
}