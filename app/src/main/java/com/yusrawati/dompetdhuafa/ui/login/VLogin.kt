package com.yusrawati.dompetdhuafa.ui.login

import android.content.Intent
import android.view.View
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataLogin
import com.yusrawati.dompetdhuafa.ui.main.VMain
import com.yusrawati.dompetdhuafa.ui.register.VRegister
import com.yusrawati.dompetdhuafa.utils.CustomToast
import com.yusrawati.dompetdhuafa.utils.Global
import com.yusrawati.dompetdhuafa.utils.SharedKey
import com.yusrawati.dompetdhuafa.utils.SharedPref
import kotlinx.android.synthetic.main.activity_login_screen.*

class VLogin : BaseActivity(), ILogin.View {
    private val mPresenter = PLogin()
    override fun getLayoutResource(): Int = R.layout.activity_login_screen

    override fun myCodeHere() {
        setupView()
    }

    override fun setupView() {
        mPresenter.attachView(this)
        btn_login.setOnClickListener {
            /**
             * TODO Cek username && password kosong atau tidak
             */
            if (et_username.text.isNotEmpty() && et_password.text.isNotEmpty()) {
                val headers = LinkedHashMap<String, String>()
                headers.put(Global.auth, Global.tokenGeneral ?: "")
                headers.put("username", et_username.text.toString())
                headers.put("password", et_password.text.toString())
                headers.put("tokendevice", FirebaseInstanceId.getInstance().token ?: throw NullPointerException())
                mPresenter.getLogin(headers)
            } else {
                onError(getString(R.string.error_empty))
            }
        }
        btn_register.setOnClickListener {
            /**
             * TODO Memanggil screen register
             */
            startActivity(Intent(applicationContext, VRegister::class.java))
        }
    }

    override fun onDialog(show: Boolean) {
        if (show) pb_loading.visibility = View.VISIBLE else pb_loading.visibility = View.GONE
    }

    override fun onError(message: String) {
        CustomToast.makeText(this, message, Toast.LENGTH_SHORT)
    }

    override fun onExpireToken() {
    }

    //TODO ketika berhasil login menyimpan token login dan mengupdate status login jdi true
    override fun onGetLogin(dataLogin: DataLogin) {
        SharedPref.saveBol(SharedKey.is_login, true)
        SharedPref.saveString(SharedKey.tokenLogin, dataLogin.response?.token_type + " " + dataLogin.response?.access_token)
        SharedPref.saveString(SharedKey.tokenRefresh, dataLogin.response?.refresh_token ?: throw Exception())
        finish()
        startActivity(Intent(applicationContext, VMain::class.java))
    }

    override fun onDetachScreen() {
    }
}
