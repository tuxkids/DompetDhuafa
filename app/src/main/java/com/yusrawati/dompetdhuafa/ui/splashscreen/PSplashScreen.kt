package com.yusrawati.dompetdhuafa.ui.splashscreen

import com.yusrawati.dompetdhuafa.base.BasePresenter
import com.yusrawati.dompetdhuafa.network.OkhttpClient
import com.yusrawati.dompetdhuafa.network.api
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 **********************************************
 * Created by ukie on 11/12/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class PSplashScreen : BasePresenter<ISplashScreen.View>(), ISplashScreen.Presenter {
    private val composite = CompositeDisposable()

    override fun detachView() {
        super.detachView()
        composite.dispose()
    }

    override fun getGeneralToken(headers: LinkedHashMap<String, String>) {
        OkhttpClient.instance?.setHeaders(headers = headers)
        composite.add(api.API_CLIENT.getGeneralToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.code() == 200) {
                        mView()?.onGetGeneralToken(it.body() ?: throw Exception())
                    }
                })
    }
}