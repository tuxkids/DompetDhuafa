package com.yusrawati.dompetdhuafa.ui.berita

import android.support.v7.widget.LinearLayoutManager
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataModelBerita
import kotlinx.android.synthetic.main.activity_berita_screen.*

class VBerita : BaseActivity() {
    override fun getLayoutResource(): Int = R.layout.activity_berita_screen

    override fun myCodeHere() {
        title = getString(R.string.berita)

        //TODO ambil data dari main
        val berita = intent.extras.getParcelable("berita") as DataModelBerita.DataBerita

        rv_list_berita.setHasFixedSize(true)
        rv_list_berita.layoutManager = LinearLayoutManager(this)
        rv_list_berita.adapter = AdapterBerita(berita.response!!)

    }

    override fun onDetachScreen() {
    }
}
