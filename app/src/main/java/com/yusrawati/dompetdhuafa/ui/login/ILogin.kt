package com.yusrawati.dompetdhuafa.ui.login

import com.yusrawati.dompetdhuafa.base.BaseMVP
import com.yusrawati.dompetdhuafa.models.DataLogin

/**
 **********************************************
 * Created by ukie on 11/12/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
interface ILogin {
    interface View : BaseMVP {
        fun onGetLogin(dataLogin: DataLogin)
    }

    interface Presenter {
        fun getLogin(headers: LinkedHashMap<String, String>)
    }
}