package com.yusrawati.dompetdhuafa.ui.splashscreen

import com.yusrawati.dompetdhuafa.base.BaseMVP
import com.yusrawati.dompetdhuafa.models.DataGeneralToken

/**
 **********************************************
 * Created by ukie on 11/12/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
interface ISplashScreen {
    interface View : BaseMVP {
        fun onGetGeneralToken(dataGeneralToken: DataGeneralToken)
    }

    interface Presenter {
        fun getGeneralToken(headers: LinkedHashMap<String, String>)
    }
}