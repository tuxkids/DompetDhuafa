package com.yusrawati.dompetdhuafa.ui.register

import android.app.Dialog
import android.support.design.widget.TextInputEditText
import android.view.View
import android.widget.Toast
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataRegister
import com.yusrawati.dompetdhuafa.support.datepicker.DatePickerDailog
import com.yusrawati.dompetdhuafa.utils.CustomToast
import com.yusrawati.dompetdhuafa.utils.DateFormatter
import com.yusrawati.dompetdhuafa.utils.Global
import kotlinx.android.synthetic.main.activity_register_screen.*
import java.util.*
import kotlin.collections.LinkedHashMap


class VRegister : BaseActivity(), IRegister.View {
    private val mPresenter = PRegister()

    private var jk = "L"

    override fun getLayoutResource(): Int = R.layout.activity_register_screen

    override fun myCodeHere() {
        title = getString(R.string.register)
        setupView()


        tet_birthday.setOnClickListener { showCalendar() }
        tet_birthday.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus)
                showCalendar()
        }

        rg_sex.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_man -> jk = "L"
                R.id.rb_woman -> jk = "P"
            }
        }

        btn_register.setOnClickListener {

            if (validate(arrayOf(tet_full_name, tet_username, tet_password, tet_retype_password, tet_phone, tet_email
                    , tet_kota, tet_district, tet_postal_code, tet_birthplace, tet_birthday, tet_job))) {
                val headers = LinkedHashMap<String, String>()
                headers.put(Global.auth, Global.tokenGeneral ?: "")
                headers.put("namalengkap", tet_full_name.text.toString())
                headers.put("tempatlahir", tet_birthplace.text.toString())
                headers.put("tgllahir", tet_birthday.text.toString())
                headers.put("jk", jk)
                headers.put("alamat", tet_address.text.toString())
                headers.put("kota", tet_kota.text.toString())
                headers.put("kecamatan", tet_district.text.toString())
                headers.put("kodepos", tet_postal_code.text.toString())
                headers.put("nomortlp", tet_phone.text.toString())
                headers.put("pekerjaan", tet_job.text.toString())
                headers.put("name", tet_username.text.toString())
                headers.put("email", tet_email.text.toString())
                headers.put("password", tet_password.text.toString())
                headers.put("passwordconfirmation", tet_retype_password.text.toString())
                mPresenter.postRegister(headers)
            } else {
                onError(getString(R.string.error_empty))
            }
        }

    }

    /**
     * TODO
     * Validasi apakah kosong atau tidak
     */
    private fun validate(fields: Array<TextInputEditText>): Boolean {
        return fields.indices
                .map { fields[it] }
                .none { it.text.toString().isEmpty() }
    }

    override fun setupView() {
        mPresenter.attachView(this)
    }

    private fun showCalendar() {
        if (tet_birthday.text.isNotEmpty())
            Global.calendar.time = DateFormatter.outputType1.parse(tet_birthday.text.toString())
        else
            Global.calendar.add(Calendar.YEAR, -20)
        val datePicker = DatePickerDailog(this, Global.calendar, object : DatePickerDailog.DatePickerListner {
            override fun onDoneButton(dateDialog: Dialog?, c: Calendar?) {
                tet_birthday.setText(DateFormatter.outputType1.format(c?.time))
                dateDialog?.dismiss()
            }

            override fun onCancelButton(dateDialog: Dialog?) {
                dateDialog?.dismiss()
            }

        })
        datePicker.setMaxDate(Global.calendar)
        datePicker.show()
    }

    override fun onDialog(show: Boolean) {
        if (show) pb_loading.visibility = View.VISIBLE else pb_loading.visibility = View.GONE
    }

    override fun onError(message: String) {
        CustomToast.makeText(this, message, Toast.LENGTH_SHORT)
    }

    override fun onExpireToken() {
    }

    override fun onPostRegister(dataRegister: DataRegister) {
        onError(dataRegister.diagnostic?.description ?: "error")
        finish()
    }

    override fun onDetachScreen() {
        mPresenter.detachView()
    }
}
