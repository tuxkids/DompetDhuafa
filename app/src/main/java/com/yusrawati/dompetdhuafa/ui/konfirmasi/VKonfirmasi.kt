package com.yusrawati.dompetdhuafa.ui.konfirmasi

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.CountDownTimer
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.orhanobut.logger.Logger
import com.tbruyelle.rxpermissions2.RxPermissions
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataHistory
import com.yusrawati.dompetdhuafa.models.Diagnostic
import com.yusrawati.dompetdhuafa.ui.main.VMain
import com.yusrawati.dompetdhuafa.utils.*
import kotlinx.android.synthetic.main.activity_konfirmasi_screen.*
import kotlinx.android.synthetic.main.custom_spinner.view.*
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.LinkedHashMap

class VKonfirmasi : BaseActivity(), IKonfirmasi.View {
    private val mPresenter = PKonfirmasi()
    private var bank: List<DataHistory.BankTujuan>? = null
    private var response: DataHistory.Response? = null

    private var countDownTImer: CountDownTimer? = null
    private var idBank = "1"


    private var rxPermissions: RxPermissions? = null

    private var filePath: String? = ""
    private var imageFile: File? = null

    private val CAMERA = 1
    private val GALLERY = 2

    override fun getLayoutResource(): Int = R.layout.activity_konfirmasi_screen

    override fun myCodeHere() {
        title = getString(R.string.konfirmasi_pembayaran)
        setupView()
    }

    override fun setupView() {
        mPresenter.attachView(this)

        bank = intent.getParcelableArrayListExtra("bank")
        response = intent.getParcelableExtra("response")
        rxPermissions = RxPermissions(this)

        //TODO setup expire time
        val currentDate = Date()
        val expireDate = DateFormatter.outputType6.parse(Global.convertTimestamp(response?.expire!!.toLong()))
        val estimateTime = expireDate.time - currentDate.time
        countDownTImer = object : CountDownTimer(estimateTime, 500) {
            @SuppressLint("DefaultLocale")
            override fun onTick(millisUntilFinished: Long) {
                val second = millisUntilFinished / 1000
                try {
                    tv_sisa_waktu.text = String.format("%02d:%02d:%02d", second / 3600,
                            second % 3600 / 60, second % 60)
                } catch (ignored: Exception) {
                }
            }

            override fun onFinish() {
                tv_sisa_waktu.text = resources.getString(R.string.payment_expired)
                tv_sisa_waktu.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimary))
            }
        }.start()

        Logger.d(Global.convertTimestamp(response?.expire!!.toLong()))


        tv_tanggal_sisa_waktu.text = DateFormatter.format(Global.convertTimestamp(response?.expire?.toLong() ?: 0), 1, 6)
        tv_order_id.text = response?.id
        tet_jenis_hewan.setText(response?.hewankurban?.nama)
        tet_harga_hewan.setText(Global.currencyFormat((response?.total!!.toInt() / response?.jml_orang!!.toInt()).toDouble()))
        tet_total_bayar.setText(Global.currencyFormat(response?.total!!))


        val adapterBank = ArrayAdapter<DataHistory.BankTujuan>(this, android.R.layout.simple_spinner_item, bank)
        adapterBank.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        cs_bank.sp_content.adapter = adapterBank
        cs_bank.sp_content.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                idBank = (bank as ArrayList<DataHistory.BankTujuan>?)!![position]?.id!!
            }
        }

        val tvTitle = TextView(this)
        tvTitle.text = "Nama Pekurban"
        tvTitle.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))

        //TODO Looping untuk menampilkan nama pekurban
        ll_nama_pekurban.addView(tvTitle)
        for (items in response?.detail!!.indices) {
            val tvName = TextView(this)
            tvName.text = "${items + 1} . ${response?.detail?.get(items)?.nama}"
            ll_nama_pekurban.addView(tvName)
        }

        iv_bukti_pembayaran.setOnClickListener {
            val options = arrayOf<CharSequence>(getString(R.string.ambil_foto), getString(R.string.pilih_dari_galeri), getString(R.string.cancel))
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.upload_bukti_pembayaran))
            builder.setItems(options) { dialog, which ->
                when (options[which]) {
                    getString(R.string.ambil_foto) -> {
                        openCamera()
                        dialog.dismiss()
                    }
                    getString(R.string.pilih_dari_galeri) -> {
                        openGallery()
                        dialog.dismiss()
                    }
                    getString(R.string.cancel) -> {
                        dialog.dismiss()
                    }
                }
            }
            builder.show()
        }

        btn_konfirmasi.setOnClickListener {
            if (imageFile == null) {
                onError("Silahkan Upload Bukti Pembayaran")
            } else {
                val headers = LinkedHashMap<String, String>()
                headers.put(Global.auth, SharedPref.getString(SharedKey.tokenLogin))
                headers.put("idtransaksi", response?.id!!)
                headers.put("banktujuan", idBank)
                headers.put("tanggaltransfer", DateFormatter.format(DateFormatter.outputType6.format(currentDate), 1, 1))
                headers.put("namarekening", tet_pemilik_rekening.text.toString())
                headers.put("nomorrekening", tet_no_rekening.text.toString())
                mPresenter.postKonfirmasiPembayaran(headers, imageFile!!)
            }
        }
    }

    override fun onDialog(show: Boolean) {
        pb_loading.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onError(message: String) {
        CustomToast.makeText(this, message, Toast.LENGTH_SHORT)
    }

    override fun onExpireToken() {
    }

    //TODO Menerima respon konfirmasi pembayaran
    override fun onPostKonfirmasiPembayaran(diagnostic: Diagnostic) {
        onError(diagnostic.description!!)

        //TODO Kembali ke home, menghapus semua activity yang sudah di buka
        val home = Intent(applicationContext, VMain::class.java)
        home.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(home)
    }

    private fun openCamera() {
        rxPermissions
                ?.request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                ?.doOnError { e -> CustomToast.makeText(this, e.toString(), Toast.LENGTH_SHORT) }
                ?.subscribe({ permission ->
                    if (permission) {
                        try {
                            imageFile = createImageFile()
                        } catch (ex: java.io.IOException) {
                            ex.printStackTrace()
                        }
                        if (imageFile != null) {
                            val photoURI: Uri = FileProvider.getUriForFile(this, "com.yusrawati.dompetdhuafa.provider", imageFile)
                            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                            val resolvedIntentActivities: List<ResolveInfo> = this.packageManager.queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY)
                            resolvedIntentActivities
                                    .map { it.activityInfo.packageName }
                                    .forEach { this.grantUriPermission(it, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION) }
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                            if (cameraIntent.resolveActivity(packageManager) != null)
                                startActivityForResult(cameraIntent, CAMERA)
                        }
                    } else {
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage(getString(R.string.kamera_tidak_diizinkan))
                        builder.setPositiveButton(getString(R.string.retry)) { dialog, _ -> dialog.dismiss() }
                        builder.setCancelable(false)
                        builder.show()
                    }
                })
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = DateFormatter.outputType1.format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        filePath = image.absolutePath
        return image
    }

    private fun setPic(imageView: ImageView, imageFile: File) {
        Glide.with(this)
                .load(imageFile)
                .asBitmap()
                .into(imageView)
        imageView.visibility = View.VISIBLE
    }

    private fun openGallery() {
        rxPermissions
                ?.request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                ?.subscribe({ permission ->
                    if (permission) {
                        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                        galleryIntent.type = "image/*"
                        startActivityForResult(galleryIntent, GALLERY)
                    } else {
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage(getString(R.string.akses_galeri_tidak_diizinkan))
                        builder.setPositiveButton(getString(R.string.retry)) { dialog, _ -> dialog.dismiss() }
                        builder.setCancelable(false)
                        builder.show()

                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA && resultCode == Activity.RESULT_OK) {
            imageFile = File(filePath)
            if (imageFile?.exists()!!) {
                Logger.d("image exist")
                Logger.d("size before compress" + imageFile?.length()!! / 1024)
                val bitmap = BitmapFactory.decodeFile(imageFile?.absolutePath)
                try {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, java.io.FileOutputStream(imageFile))
                    val options = BitmapFactory.Options()
                    options.inJustDecodeBounds = true
                    options.inSampleSize = 5
                    BitmapFactory.decodeFile(imageFile?.absolutePath!!, options)

                    setPic(iv_bukti_pembayaran, imageFile!!)
                    Logger.d("size after compress" + imageFile?.length()!! / 1024)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        } else if (requestCode == GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                val selectedImage = data?.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursorImage = this.contentResolver.query(selectedImage,
                        filePathColumn, null, null, null)
                if (cursorImage != null) {
                    cursorImage.moveToFirst()
                    val columnIndex = cursorImage.getColumnIndex(filePathColumn[0])
                    val picturePath = cursorImage.getString(columnIndex)
                    imageFile = File(picturePath)
                    cursorImage.close()
                }

                Logger.d("size before compress" + imageFile?.length()!! / 1024)

                val bitmap = BitmapFactory.decodeFile(imageFile?.absolutePath!!)
                try {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, java.io.FileOutputStream(imageFile))
                    val options = BitmapFactory.Options()
                    options.inJustDecodeBounds = true
                    options.inSampleSize = 5
                    BitmapFactory.decodeFile(imageFile?.absolutePath!!, options)
                    setPic(iv_bukti_pembayaran, imageFile!!)
                    Logger.d("size after compress" + imageFile?.length()!! / 1024)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } catch (e: Exception) {        //for xiaomi device
                val uri = data?.data
                imageFile = java.io.File(uri?.path)
                val selectedImage = getImageContentUri(this, imageFile!!)
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                val cursorImage = this.contentResolver.query(selectedImage,
                        filePathColumn, null, null, null)
                if (cursorImage != null) {
                    cursorImage.moveToFirst()
                    val columnIndex = cursorImage.getColumnIndex(filePathColumn[0])
                    val picturePath = cursorImage.getString(columnIndex)
                    imageFile = File(picturePath)
                    cursorImage.close()
                }

                Logger.d("size before compress" + imageFile?.length()!! / 1024)

                val bitmap = BitmapFactory.decodeFile(imageFile?.absolutePath!!)
                try {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, java.io.FileOutputStream(imageFile))
                    val options = BitmapFactory.Options()
                    options.inJustDecodeBounds = true
                    options.inSampleSize = 5
                    BitmapFactory.decodeFile(imageFile?.absolutePath!!, options)
                    setPic(iv_bukti_pembayaran, imageFile!!)
                    Logger.d("size after compress" + imageFile!!.length() / 1024)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            filePath = imageFile?.absolutePath!!
            imageFile = java.io.File(imageFile?.absolutePath!!)
        }
    }

    private fun getImageContentUri(context: Context, imageFile: File): Uri? {
        val filePath = imageFile.absolutePath
        val cursor = context.contentResolver.query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                arrayOf(MediaStore.Images.Media._ID),
                MediaStore.Images.Media.DATA + "=? ",
                arrayOf(filePath), null)

        if (cursor != null && cursor.moveToFirst()) {
            val id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID))
            val baseUri = Uri.parse("content://media/external/images/media")
            cursor.close()
            return Uri.withAppendedPath(baseUri, "" + id)
        } else {
            if (imageFile.exists()) {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.DATA, filePath)
                return context.contentResolver.insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            } else {
                return null
            }
        }
    }

    override fun onDetachScreen() {
        mPresenter.detachView()
        countDownTImer?.cancel()
    }
}
