package com.yusrawati.dompetdhuafa.ui.register

import com.yusrawati.dompetdhuafa.base.BasePresenter
import com.yusrawati.dompetdhuafa.network.OkhttpClient
import com.yusrawati.dompetdhuafa.network.api
import com.yusrawati.dompetdhuafa.utils.Global
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 **********************************************
 * Created by ukie on 11/12/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class PRegister : BasePresenter<IRegister.View>(), IRegister.Presenter {
    private val composite = CompositeDisposable()
    override fun postRegister(headers: LinkedHashMap<String, String>) {
        OkhttpClient.instance?.setHeaders(headers)
        composite.add(api.API_CLIENT.postRegister()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { mView()?.onDialog(true) }
                .doOnComplete { mView()?.onDialog(false) }
                .doOnError { mView()?.onDialog(false) }
                .subscribe {
                    if (it.code() == 200) {
                        mView()?.onPostRegister(it.body() ?: throw Exception())
                    } else
                        mView()?.onError(Global.parseError(it.errorBody() ?: throw Exception()))
                })
    }

    override fun detachView() {
        super.detachView()
        composite.dispose()
    }
}