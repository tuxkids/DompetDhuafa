package com.yusrawati.dompetdhuafa.ui.history

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataHistory
import com.yusrawati.dompetdhuafa.utils.CustomToast
import com.yusrawati.dompetdhuafa.utils.Global
import com.yusrawati.dompetdhuafa.utils.SharedKey
import com.yusrawati.dompetdhuafa.utils.SharedPref
import kotlinx.android.synthetic.main.activity_history_screen.*

class VHistory : BaseActivity(), IHistory.View {
    private val mPresenter = PHistory()
    override fun getLayoutResource(): Int = R.layout.activity_history_screen

    override fun myCodeHere() {
        title = getString(R.string.riwayat_transaksi)
        setupView()
    }

    override fun setupView() {
        mPresenter.attachView(this)

        //TODO request data history kurban
        val headers = LinkedHashMap<String, String>()
        headers.put(Global.auth, SharedPref.getString(SharedKey.tokenLogin))
        mPresenter.getHistroy(headers)
    }

    override fun onDialog(show: Boolean) {
        pb_loading.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onError(message: String) {
        CustomToast.makeText(this, message, Toast.LENGTH_SHORT)
    }

    override fun onExpireToken() {
    }

    //TODO Mendapat respon data histroy kurban
    override fun onGetHistory(dataHistory: DataHistory.History) {
        val adapterHistory = AdapterHistory(dataHistory)
        rv_history.setHasFixedSize(true)
        rv_history.layoutManager = LinearLayoutManager(this)
        rv_history.adapter = adapterHistory
    }

    override fun onDetachScreen() {
        mPresenter.detachView()
    }
}
