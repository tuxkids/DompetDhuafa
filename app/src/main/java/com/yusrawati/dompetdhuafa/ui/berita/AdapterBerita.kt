package com.yusrawati.dompetdhuafa.ui.berita

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.models.DataModelBerita
import com.yusrawati.dompetdhuafa.ui.berita.detail.VDetailBerita
import kotlinx.android.synthetic.main.activity_berita_item.view.*

/**
 **********************************************
 * Created by ukie on 11/16/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class AdapterBerita(val dataBerita: List<DataModelBerita.Response>) : RecyclerView.Adapter<AdapterBerita.MyHolder>() {

    /**
     * TODO Parameter perulangan berita
     */
    override fun getItemCount(): Int = dataBerita.size

    /**
     * TODO layout item berita
     */
    override
    fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) =
            MyHolder(LayoutInflater.from(parent?.context).inflate(R.layout.activity_berita_item, parent, false))

    override fun onBindViewHolder(holder: MyHolder?, position: Int) {
        holder?.bindHistory(dataBerita[position])
    }

    /**
     * TODO Looping untuk menampilkan data per berita
     */
    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindHistory(dataHistory: DataModelBerita.Response) {
            Glide.with(itemView.context)
                    .load(dataHistory.gambar)
                    .thumbnail(0.5F)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.iv_news)

            itemView.tv_title.text = dataHistory.judul
            itemView.tv_date.text =dataHistory.tanggal
            itemView.tv_writer.text = dataHistory.sumber
            itemView.cv_news.setOnClickListener {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val detailNews = Intent(itemView.context.applicationContext, VDetailBerita::class.java)
                    detailNews.putExtra("id", dataHistory.id)
                    detailNews.putExtra("response", dataHistory)

                    val image = Pair<View, String>(itemView.iv_news, itemView.iv_news.transitionName)
                    val title = Pair<View, String>(itemView.tv_title, itemView.tv_title.transitionName)
                    val date = Pair<View, String>(itemView.tv_date, itemView.tv_date.transitionName)
                    val writer = Pair<View, String>(itemView.tv_writer, itemView.tv_writer.transitionName)

                    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            itemView.context as Activity,
                            image, title, date, writer
                    )
                    itemView.context.startActivity(detailNews, options.toBundle())
                } else {
                    val detailNews = Intent(itemView.context.applicationContext, VDetailBerita::class.java)
                    detailNews.putExtra("id", dataHistory.id)
                    detailNews.putExtra("response", dataHistory)
                    itemView.context.startActivity(detailNews)
                }
            }
        }
    }
}
