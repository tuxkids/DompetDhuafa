package com.yusrawati.dompetdhuafa.ui.main

import com.yusrawati.dompetdhuafa.base.BaseMVP
import com.yusrawati.dompetdhuafa.models.DataModelBerita

/**
 **********************************************
 * Created by ukie on 12/9/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
interface IMain {
    interface View : BaseMVP {
        fun onGetBerita(dataBerita: DataModelBerita.DataBerita)
    }

    interface Presenter {
        fun getBerita(headers: LinkedHashMap<String, String>)
    }
}