package com.yusrawati.dompetdhuafa.ui.register

import com.yusrawati.dompetdhuafa.base.BaseMVP
import com.yusrawati.dompetdhuafa.models.DataRegister

/**
 **********************************************
 * Created by ukie on 11/12/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
interface IRegister {
    interface View : BaseMVP {
        fun onPostRegister(dataRegister: DataRegister)
    }

    interface Presenter {
        fun postRegister(headers: LinkedHashMap<String, String>)
    }
}