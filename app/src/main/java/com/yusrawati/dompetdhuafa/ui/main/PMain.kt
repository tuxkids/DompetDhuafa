package com.yusrawati.dompetdhuafa.ui.main

import com.yusrawati.dompetdhuafa.base.BasePresenter
import com.yusrawati.dompetdhuafa.network.OkhttpClient
import com.yusrawati.dompetdhuafa.network.api
import com.yusrawati.dompetdhuafa.utils.Global
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 **********************************************
 * Created by ukie on 12/9/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class PMain : BasePresenter<IMain.View>(), IMain.Presenter {
    private val composite = CompositeDisposable()
    override fun detachView() {
        super.detachView()
        composite.dispose()
    }

    override fun getBerita(headers: LinkedHashMap<String, String>) {
        OkhttpClient.instance?.setHeaders(headers)
        composite.add(api.API_CLIENT.getBerita()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { mView()?.onDialog(true) }
                .doOnComplete { mView()?.onDialog(false) }
                .doOnError { mView()?.onDialog(false) }
                .subscribe {
                    if (it.code() == 200) {
                        mView()?.onGetBerita(it.body() ?: throw Exception())
                    } else
                        mView()?.onError(Global.parseError(it.errorBody() ?: throw Exception()))
                })

    }
}