package com.yusrawati.dompetdhuafa.ui.history

import com.yusrawati.dompetdhuafa.base.BaseMVP
import com.yusrawati.dompetdhuafa.models.DataHistory

/**
 **********************************************
 * Created by ukie on 11/17/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
interface IHistory {
    interface View : BaseMVP {
        fun onGetHistory(dataHistory: DataHistory.History)
    }

    interface Presenter {
        fun getHistroy(headers:LinkedHashMap<String,String>)
    }
}