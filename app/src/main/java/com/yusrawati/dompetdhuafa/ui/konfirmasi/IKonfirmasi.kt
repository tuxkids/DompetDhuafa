package com.yusrawati.dompetdhuafa.ui.konfirmasi

import com.yusrawati.dompetdhuafa.base.BaseMVP
import com.yusrawati.dompetdhuafa.models.Diagnostic
import java.io.File

/**
 **********************************************
 * Created by ukie on 11/21/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
interface IKonfirmasi {
    interface View : BaseMVP {
        fun onPostKonfirmasiPembayaran(diagnostic: Diagnostic)
    }

    interface Presenter {
        fun postKonfirmasiPembayaran(headers: LinkedHashMap<String, String>, file: File)
    }
}