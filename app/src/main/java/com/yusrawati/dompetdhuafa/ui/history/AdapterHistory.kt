package com.yusrawati.dompetdhuafa.ui.history

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Parcelable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.models.DataHistory
import com.yusrawati.dompetdhuafa.ui.konfirmasi.VKonfirmasi
import com.yusrawati.dompetdhuafa.utils.DateFormatter
import com.yusrawati.dompetdhuafa.utils.Global
import kotlinx.android.synthetic.main.activity_history_item.view.*
import java.util.*

/**
 **********************************************
 * Created by ukie on 11/16/17 with ♥
 * (>’_’)> email : ukie.tux@gmail.com
 * github : https://www.github.com/tuxkids <(’_’<)
 **********************************************
 * © 2017 | All Right Reserved
 */
class AdapterHistory(val dataHistory: DataHistory.History) : RecyclerView.Adapter<AdapterHistory.MyHolder>() {
    override fun getItemCount(): Int = dataHistory.response?.size ?: 0

    override
    fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) =
            MyHolder(LayoutInflater.from(parent?.context).inflate(R.layout.activity_history_item, parent, false))

    override fun onBindViewHolder(holder: MyHolder?, position: Int) {
        holder?.bindHistory(dataHistory, dataHistory.response!![position])
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindHistory(dataHistory: DataHistory.History, response: DataHistory.Response) {
            itemView.tv_jumlah.text = "${response.jml_orang} orang"
            itemView.tv_jumlah_bayar.text = Global.currencyFormat((response.total ?: 0.0 * response.jml_orang?.toInt()!!))
            itemView.tv_order_id.text = response.id
            itemView.tv_tanggal.text = DateFormatter.format(response.tanggal ?: "", 1, 2)
            itemView.tv_nama_hewan.text = response.hewankurban?.nama
            itemView.tv_status.text = response.status

//            0 itu di tolak, 1 menunggu pembayaran, 2 menunggu verikasi admin, 3 terverifikasi
            var background: Drawable? = null
            //TODO seleksi untuk warna background berdasarkan status
            when (response.fl_status) {
                0 -> background = ContextCompat.getDrawable(itemView.context, R.drawable.bg_round_red)
                1, 2 -> background = ContextCompat.getDrawable(itemView.context, R.drawable.bg_round_yellow)
                3 -> background = ContextCompat.getDrawable(itemView.context, R.drawable.bg_round_green)
            }
            itemView.tv_status.background = background

            //TODO ketika status ==1 menuju konfirmasi pembayaran
            if (response.fl_status == 1)
                itemView.setOnClickListener {
                    val konfirmasi = Intent(itemView.context.applicationContext, VKonfirmasi::class.java)
                    konfirmasi.putExtra("response", response)
                    konfirmasi.putParcelableArrayListExtra("bank", dataHistory.bank_tujuan as ArrayList<out Parcelable>)
                    itemView.context.startActivity(konfirmasi)
                }
        }
    }
}