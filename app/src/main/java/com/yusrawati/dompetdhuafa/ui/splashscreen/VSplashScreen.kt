package com.yusrawati.dompetdhuafa.ui.splashscreen

import android.content.Intent
import com.google.firebase.FirebaseApp
import com.yusrawati.dompetdhuafa.BuildConfig
import com.yusrawati.dompetdhuafa.R
import com.yusrawati.dompetdhuafa.base.BaseActivity
import com.yusrawati.dompetdhuafa.models.DataGeneralToken
import com.yusrawati.dompetdhuafa.ui.main.VMain
import com.yusrawati.dompetdhuafa.utils.Global

class VSplashScreen : BaseActivity(), ISplashScreen.View {
    private val mPresenter = PSplashScreen()
    override fun getLayoutResource(): Int = R.layout.activity_splash_screen

    override fun myCodeHere() {
        setupView()
    }

    override fun setupView() {
        mPresenter.attachView(this)
        FirebaseApp.initializeApp(this)

        val headers = LinkedHashMap<String, String>()
        headers.put("clientid", BuildConfig.ClientID)
        headers.put("clientsecret", BuildConfig.ClientSecret)
        mPresenter.getGeneralToken(headers)
    }

    override fun onDialog(show: Boolean) {
    }

    override fun onError(message: String) {
    }

    override fun onExpireToken() {
    }

    override fun onGetGeneralToken(dataGeneralToken: DataGeneralToken) {
        Global.tokenGeneral = "${dataGeneralToken.response?.token_type} ${dataGeneralToken.response?.access_token}"
        val i = Intent(applicationContext, VMain::class.java)
        startActivity(i)
        // close this activity
        finish()
    }

    override fun onDetachScreen() {
        mPresenter.detachView()
    }
}
