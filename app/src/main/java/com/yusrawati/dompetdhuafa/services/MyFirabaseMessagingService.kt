package com.yusrawati.dompetdhuafa.services


import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.yusrawati.dompetdhuafa.R
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * ----------------------------------------------
 * Created by ukieTux on 12/31/16 with ♥ .
 * @email : ukie.tux@gmail.com
 * @github : https://www.github.com/tuxkids
 * * ----------------------------------------------
 * * © 2016 | All Rights Reserved
 */

class MyFirebaseMessagingService : FirebaseMessagingService() {


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        /**
         * Receive from console firebase
         */
        if (remoteMessage?.data?.isNotEmpty() ?: throw NullPointerException()) {
            try {
                val jsonObject = JSONObject(remoteMessage.data)
                sendNotification(jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    private fun sendNotification(jsonObject: JSONObject) {
        try {
            //get root json
            val messageTitle = jsonObject.getString("title")
            val messageBody = jsonObject.getString("message")
//            val type = jsonObject.getString("type")

            var notifIntent: Intent? = null
//            when (type.toLowerCase()) {
//                "system" -> {
//                    notifIntent = Intent(applicationContext, VMainScreen::class.java)
//                    notifIntent.putExtra("notifIntent", true)
//                    notifIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                }
//                "pendaftaran" -> {
//                    notifIntent = Intent(applicationContext, VHistoryOrder::class.java)
//                    notifIntent.putExtra("notifIntent", true)
//                    notifIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                }
//                "berita" -> {
//                    notifIntent = Intent(applicationContext, VNews::class.java)
//                    notifIntent.putExtra("notifIntent", true)
//                    notifIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                }
//                "antrian" -> {
//                    notifIntent = Intent(applicationContext, VQueue::class.java)
//                    notifIntent.putExtra("notifIntent", true)
//                    notifIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                }
//            }

            val pendingIntent = PendingIntent.getActivity(applicationContext, 0, notifIntent, PendingIntent.FLAG_ONE_SHOT)

            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notifyBuilder = NotificationCompat.Builder(applicationContext, "notif")
            val inboxStyle = NotificationCompat.InboxStyle()
            inboxStyle.addLine(messageBody)
            val notification = notifyBuilder
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setTicker(messageTitle)
                    .setAutoCancel(true)
                    .setContentTitle(messageTitle)
                    .setContentIntent(pendingIntent)
                    .setSound(defaultSoundUri)
                    .setStyle(inboxStyle)
                    .setWhen(System.currentTimeMillis())
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                    .setContentText(messageBody)
                    .build()
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val m = (Date().time / 1000L % Integer.MAX_VALUE).toInt()
            notificationManager.notify(m, notification)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
